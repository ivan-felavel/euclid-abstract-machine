#encoding: utf-8
"""
Máquina abstracta de Euclides
"""

import math
import sys
import random
import re
import turtle as t


CIRCLES_LABELS = {}
LINES_LABELS = {}
INTERSECTION_LABELS = {}
POINTS_LIST = []
POINTS = {}

INSTRUCTIONS = []

def P(list_of_points):
    """
    Dibuja los puntos de la lista
    """
    for point in list_of_points:
        t.up()
        t.setposition(point[0], point[1])
        t.down()
        t.dot(5)

def C(center, point):
    """
    Dibuja círculo
    """
    x1 = center[0]
    y1 = center[1]
    x2 = point[0]
    y2 = point[1]
    angle = math.degrees(math.atan2(y1 - y2, x1 - x2))
    if angle < 0:
        angle += 360.0
    t.up()
    t.setposition(x2, y2)
    t.down()
    t.seth(angle)
    t.right(90)
    radius = math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)
    t.circle(radius)

def LC(center, point, label):
    """
    Etiqueta círculo
    """
    global CIRCLES_LABELS
    CIRCLES_LABELS[label] = (center, point)

def L(point_p, point_q):
    """
    Dibuja línea
    """
    x1 = point_p[0]
    y1 = point_p[1]
    x2 = point_q[0]
    y2 = point_q[1]
    angle = math.degrees(math.atan2(y1 - y2, x1 - x2))
    if angle < 0:
        angle += 360.0
    distance = math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)
    t.up()
    t.setposition(x2, y2)
    t.seth(angle)
    t.bk(1000)
    t.down()
    t.fd(1000 * 2 + distance)

def LL(point_p, point_q, label):
    """
    Etiqueta línea
    """
    global LINES_LABELS
    LINES_LABELS[label] = (point_p, point_q)

def LP(object_1, object_2, label_a=None, label_b=None):
    global LINES_LABELS, CIRCLES_LABELS, INTERSECTION_LABELS

    #####################################
    ##      Una recta y un círculo     ##
    #####################################
    
    if object_1 in LINES_LABELS and object_2 in CIRCLES_LABELS:
        one_line_one_circle(object_1, object_2, label_a, label_b)
    if object_1 in CIRCLES_LABELS and object_2 in LINES_LABELS:
        one_line_one_circle(object_2, object_1, label_a, label_b)

    #####################################
    ##          2 círculos             ##
    #####################################
        
    if object_1 in CIRCLES_LABELS and object_2 in CIRCLES_LABELS:
        two_circles(object_1, object_2, label_a, label_b)

    #####################################
    ##          2 rectas               ##
    #####################################
        
    if object_1 in LINES_LABELS and object_2 in LINES_LABELS:
        two_lines(object_1, object_2, label_a)

    #####################################
    ##          Circulo y Punto        ##
    #####################################
    if object_1[0] == 'Q' and object_2[0] == 'P':
        POINTS[label_a] = POINTS[object_2]  
    if object_1[0] == 'P' and object_2[0] == 'Q':
        POINTS[label_a] = POINTS[object_1]  

def D(label_a):
    global CIRCLES_LABELS
    global LINES_LABELS
    CIRCLES_LABELS.pop(label_a, None)
    LINES_LABELS.pop(label_a, None)
    POINTS.pop(label_a, None)

def INC(point, circle):
    global CIRCLES_LABELS
    if circle in CIRCLES_LABELS:
        center = CIRCLES_LABELS[circle][0]
        border = CIRCLES_LABELS[circle][1]
        radius = (center[0] - border[0]) ** 2 + (center[1] - border[1]) ** 2
        distance_from_center_to_point = (center[0] - point[0]) ** 2 + (center[1] - point[1]) ** 2
        if distance_from_center_to_point <= radius:
                return True
    return False

def one_line_one_circle(line, circle, label_a=None, label_b=None):
    global LINES_LABELS, CIRCLES_LABELS, INTERSECTION_LABELS, POINTS

    x1 = LINES_LABELS[line][0][0]
    y1 = LINES_LABELS[line][0][1]
    x2 = LINES_LABELS[line][1][0]
    y2 = LINES_LABELS[line][1][1]
    m = float(y2 - y1) / float(x2 - x1)
    b = y1 - m * x1
    p = CIRCLES_LABELS[circle][0][0]
    q = CIRCLES_LABELS[circle][0][1]
    radius = math.sqrt((CIRCLES_LABELS[circle][0][0] - CIRCLES_LABELS[circle][1][0]) ** 2 + (CIRCLES_LABELS[circle][0][1] - CIRCLES_LABELS[circle][1][1]) ** 2)
    A = m ** 2 + 1
    B = 2 * (m * b - m * q - p)
    C = q ** 2 - radius ** 2 + p ** 2 - 2 * b * q + b ** 2
    point1 = None
    point2 = None
    px = None
    py = None
    if B ** 2 - 4 * A * C < 0:
        sys.exit("[Una recta, un círculo] No se intersectan")
    elif B ** 2 - 4 * A * C == 0:
        px = ((- B) / (2 * A))
        py = m * px + b
        point1 = (px, py)
        POINTS[label_a] = point1
        POINTS[label_b] = point1
        INTERSECTION_LABELS[label_a] = (point1, line, circle)
        return True
    else:
        px = (-B + math.sqrt(B ** 2 - 4 * A * C)) / (2 * A)
        py = m * px + b
        point1 = (px, py)
        px = (-B - math.sqrt(B ** 2 - 4 * A * C)) / (2 * A)
        py = m * px + b
        point2 = (px, py)
        POINTS[label_a] = point1
        POINTS[label_b] = point2
        INTERSECTION_LABELS[label_a] = (point1, line, circle)
        INTERSECTION_LABELS[label_b] = (point2, line, circle)
        return True

def two_circles(circle1, circle2, label_a=None, label_b=None):
    global CIRCLES_LABELS, INTERSECTION_LABELS
    a = CIRCLES_LABELS[circle1][0][0]
    b = CIRCLES_LABELS[circle1][0][1]
    r = math.sqrt((CIRCLES_LABELS[circle1][0][0] - CIRCLES_LABELS[circle1][1][0]) ** 2 + (CIRCLES_LABELS[circle1][0][1] - CIRCLES_LABELS[circle1][1][1]) ** 2)
    
    c = CIRCLES_LABELS[circle2][0][0]
    d = CIRCLES_LABELS[circle2][0][1]
    s = math.sqrt((CIRCLES_LABELS[circle2][0][0] - CIRCLES_LABELS[circle2][1][0]) ** 2 + (CIRCLES_LABELS[circle2][0][1] - CIRCLES_LABELS[circle2][1][1]) ** 2)
    e = c - a                       
    f = d - b                  
    point1 = None
    point2 = None
    p = math.sqrt(e*e + f*f)                 
    if p == 0:
        sys.exit("[2 círculos] No se intersectan")
    k = (p * p + r * r - s * s)/(2 * p)
    w = r * r - k * k
    if w < 0:
        sys.exit("[2 círculos] No se intersectan")
    x = a + e * float(k / p) + float(f / p) * math.sqrt(w)
    y = b + f * float(k / p) - float(e / p) * math.sqrt(w)
    point1 = (x, y)
    x = a + e * float(k / p) - float(f / p) * math.sqrt(w)
    y = b + f * float(k / p) + float(e / p) * math.sqrt(w)
    point2 = (x, y)
    POINTS[label_a] = point1
    POINTS[label_b] = point2
    INTERSECTION_LABELS[label_a] = (point1, circle1, circle2)
    INTERSECTION_LABELS[label_b] = (point2, circle1, circle2)
    return True

def two_lines(line1, line2, label_a):
    global LINES_LABELS, INTERSECTION_LABELS
    x1 = LINES_LABELS[line1][0][0]
    y1 = LINES_LABELS[line1][0][1]
    x2 = LINES_LABELS[line1][1][0]
    y2 = LINES_LABELS[line1][1][1]
    x3 = LINES_LABELS[line2][0][0]
    y3 = LINES_LABELS[line2][0][1]
    x4 = LINES_LABELS[line2][1][0]
    y4 = LINES_LABELS[line2][1][1]
    det = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)
    if det == 0:
        sys.exit("[Dos rectas] No se intersectan")
    px = float((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / float(det)
    py = float((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / float(det)
    POINTS[label_a] = (px, py)
    INTERSECTION_LABELS[label_a] = ((px, py), line1, line2)    
    return True

def read_file_and_parse(file="test.in"):
    global INSTRUCTIONS
    instructions_file = open(file, "r")
    inst = None
    for line in instructions_file:
        line = line.replace(" ", "")
        line = line.replace("\n", "")
        inst = re.split('[^\w]', line)
        if inst[0].isdigit():
            inst.pop(0)
        if inst[-1] == "":
            inst.pop(-1)
        INSTRUCTIONS.append(inst)

t.speed(9)
random.seed()

read_file_and_parse(str(sys.argv[1]))

i = 0

while i < len(INSTRUCTIONS):
    print "Instruccion", i+1
    if INSTRUCTIONS[i][0].upper() == 'P':
        aux_points = []
        for punto in INSTRUCTIONS[i][1:]:
            if punto not in POINTS:
                POINTS_LIST.append((random.randint(-70, 70), random.randint(-70, 70)))
                POINTS[punto] = POINTS_LIST[-1]
            aux_points.append(POINTS[punto])
        P(aux_points)
    elif INSTRUCTIONS[i][0].upper() == 'C':
        center = INSTRUCTIONS[i][1]
        border = INSTRUCTIONS[i][2]
        if center in POINTS and border in POINTS:
            C(POINTS[center], POINTS[border])
        else:
            print "Instrucción", i + 1
            print "Error dibujar circulo con puntos %s y %s" %(center, border)
    elif INSTRUCTIONS[i][0].upper() == 'LC':
        center = INSTRUCTIONS[i][1]
        border = INSTRUCTIONS[i][2]
        if center in POINTS and border in POINTS:
            LC(POINTS[center], POINTS[border], INSTRUCTIONS[i][3])
        else:
            print "Instrucción", i + 1
            print "Error etiquetar circulo con puntos %s y %s" %(center, border)

    elif INSTRUCTIONS[i][0].upper() == 'L':
        point1 = INSTRUCTIONS[i][1]
        point2 = INSTRUCTIONS[i][2]
        if point1 in POINTS and point2 in POINTS:
            L(POINTS[point1], POINTS[point2])
        else:
            print "Instrucción", i + 1
            print "Error dibujar linea con puntos %s y %s" %(point1, point2)

    elif INSTRUCTIONS[i][0].upper() == 'LL':
        point1 = INSTRUCTIONS[i][1]
        point2 = INSTRUCTIONS[i][2]
        if point1 in POINTS and point2 in POINTS:
            LL(POINTS[point1], POINTS[point2], INSTRUCTIONS[i][3])
        else:
            print "Instrucción", i + 1
            print "Error etiquetar linea con puntos %s y %s" %(point1, point2)

    elif INSTRUCTIONS[i][0].upper() == 'LP':
        object_1 = INSTRUCTIONS[i][1]
        object_2 = INSTRUCTIONS[i][2]
        if len(INSTRUCTIONS[i]) > 4:
            LP(object_1, object_2, INSTRUCTIONS[i][3], INSTRUCTIONS[i][4])
        else:
            LP(object_1, object_2, INSTRUCTIONS[i][3])

    elif INSTRUCTIONS[i][0].upper() == 'D':
        D(INSTRUCTIONS[i][1])
    elif INSTRUCTIONS[i][0].upper() == 'INC':
        point = INSTRUCTIONS[i][1]
        circle = INSTRUCTIONS[i][2]
        if point in POINTS:
            if INC(POINTS[point], circle):
                i = int(INSTRUCTIONS[i][3]) - 2
                print "Salto a instrucción %d" %(i+2)
            else:
                print "No salto"
    elif INSTRUCTIONS[i][0].upper() == "COLOR":
        t.color("red")
    else:
        print "Instrucción %d no se puede interpretar. Se pasa a la siguiente instrucción." %(i + 1)
    i += 1
raw_input()
print POINTS