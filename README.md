Simulador de la máquina abstracta de Euclides
===========================

Un  simulador de la máquina abstracta de Euclides elaborado en **Python 2.7** utilizando el módulo de ```Turtle```.

### Características del código
-----------------
Las instrucciones de la máquina se implemetaron en funciones separadas, cada una con el nombre que tienen en el documento de [The Euclid Abstract Machine: Trisection of the Angle and the Halting Problem](https://link.springer.com/chapter/10.1007%2F11839132_16). 
Cada función recibe como argumentos etiquetas que sirven como llaves en un diccionario. Dependiendo del objeto que deba recibir esa función se obtiene el valor del diccionario correspondiente. Son tres los importantes, ```POINTS```, ```CIRCLES_LABELS``` y ```LINES_LABELS```. En los tres, la llave es una cadena, y el valor es una tupla con las coordenadas de los puntos.

Las métodos que representan las operaciones son explícitas en su funcionamiento, solo la función ```LP```, se tuvo que dividir en varios casos de acuerdo al tipo de objetos que se intersectan. 

El flujo principal del programa va de la siguiente forma:
* Se lee a partir de un archivo el conjunto de instrucciones que ejecutara la máquina
* Se almacena el conjunto de instrucciones en una lista
* Cada elemento de la lista es una lista de cadenas, la primer cadena es el nombre de la operacíón, los elementos restantes son los argumentos de la operación.
* Con un bucle se lee cada instrucción y se ejecuta.


#### Archivo de entrada
---------------
El archivo de entrada está compuesto por las instrucciones que debe seguir el programa, las condiciones del archivo son:
* Después de cada instrucción hay un salto de línea.
* Cada elemento de la instrucción debe ir separado por un caractér ni alfanumérico ni espacios en blanco, es necesario un separador.


#### Ejemplo de entrada válidas:

```
1:P(P1,P2,P3)
2: L (P1, P2)
3: LL (P1, P2; R1)
4: L (P1, P3)
5: LL (P1, P3; R2)
6: C (P1, P2)
7: LC (P1, P2; Q1)
8. LP (Q1, R2; P4, P5)
9: C (P3, P5)
10: LC (P3, P5; Q2)
```
#### Salida
-------------
Durante la ejecución se mostrarán los resultados en la ventana de **Turtle Graphics** y en la consola se muestra la instrucción que está corriendo.

#### Ejecución del programa
---------
Para correr el programa solo es necesrio ejecutar la siguiente instrucción:
```bash
python euclid_abs_mach.py ArchivoDeEntrada
```
